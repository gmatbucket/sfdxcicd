sfdx force:auth:jwt:grant --clientid $CONSUMER_KEY --jwtkeyfile keys/server.key --username $HUB_USERNAME --setdefaultdevhubusername --setalias cicdHub --instanceurl $SFDC_PROD_URL
sfdx force:org:create -s -f config/project-scratch-def.json -a ciscratch
sfdx force:source:push
sfdx force:apex:test:run -y -c -r human
sfdx force:org:delete -u ciscratch